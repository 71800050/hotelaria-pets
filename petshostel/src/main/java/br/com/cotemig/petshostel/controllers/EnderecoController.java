/**
 * 
 */
package br.com.cotemig.petshostel.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cotemig.petshostel.entities.Endereco;
import br.com.cotemig.petshostel.repositories.EnderecoRepository;

/**
 * @author Brian Alves Pereira / Luiz Diamantino
 *
 */
@RestController
@RequestMapping(value = "/enderecos")
public class EnderecoController {

	@Autowired
	private EnderecoRepository repository;

	@GetMapping(value = "/{id}")
	public ResponseEntity<Endereco> findById(@PathVariable Long id) {
		Endereco endereco = repository.findById(id).get();
		return ResponseEntity.ok(endereco);
	}

	@GetMapping
	public ResponseEntity<List<EnderecoDTO>> findAll() {
		List<EnderecoDTO> list = enderecoService.findAll();

		return ResponseEntity.ok(list);
	}

	@PostMapping
	public ResponseEntity<EnderecoDTO> insert(@RequestBody EnderecoInsertDTO dto) {
		try {
			EnderecoDTO obj = enderecoService.insert(dto);
			URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(obj.getId()).toUri();

			return ResponseEntity.created(uri).body(obj);
		} catch (ServiceException e) {
			return ResponseEntity.unprocessableEntity().build();
		}
	}

}
