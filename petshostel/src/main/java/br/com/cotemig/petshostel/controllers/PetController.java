/**
 * 
 */
package br.com.cotemig.petshostel.controllers;

import java.net.URI;
import java.util.List;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.com.cotemig.petshostel.dto.PetDTO;
import br.com.cotemig.petshostel.dto.PetInsertDTO;
import br.com.cotemig.petshostel.services.PetService;

/**
 * @author Luiz Diamantino
 *
 */


@RestController
@RequestMapping(value = "/pet")
public class PetController {

	@Autowired
	private PetService petService;

	@GetMapping(value = "/{id}")
	public ResponseEntity<PetDTO> findById(@PathVariable Long id) {
		PetDTO pet = petService.findById(id);

		return ResponseEntity.ok(pet);
	}

	@GetMapping
	public ResponseEntity<List<PetDTO>> findAll() {
		List<PetDTO> list = petService.findAll();

		return ResponseEntity.ok(list);
	}

	@PostMapping
	public ResponseEntity<PetDTO> insert(@RequestBody PetInsertDTO dto) {
		try {
			PetDTO obj = petService.insert(dto);
			URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(obj.getId()).toUri();

			return ResponseEntity.created(uri).body(obj);
		} catch (ServiceException e) {
			return ResponseEntity.unprocessableEntity().build();
		}
	}

}
