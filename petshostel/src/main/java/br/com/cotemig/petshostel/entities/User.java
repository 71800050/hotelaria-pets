/**
 * 
 */
package br.com.cotemig.petshostel.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author José Eustaquio Muniz
 *
 */
@Entity
@Table(name = "tb_user")
public abstract class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Long id;
	private String cpf;
	private String name;
	private String gender;      // genero
	private Long age;           // idade
	private String phone;       // telefone
	private String address;   // endereco
	private String email;       // email
	private String password;    // senha
	private String user_level;  // nivel_usuario

	public User() {}

	/**
	 * @param id
	 * @param cpf
	 * @param name
	 * @param gender
	 * @param age
	 * @param phone
	 * @param address
	 * @param email
	 * @param password
	 * @param user_level
	 */
	public User(Long id, String cpf, String name, String gender, Long age, String phone, String address, String email,
			String password, String user_level) {
		this.id = id;
		this.cpf = cpf;
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.phone = phone;
		this.address = address;
		this.email = email;
		this.password = password;
		this.user_level = user_level;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the age
	 */
	public Long getAge() {
		return age;
	}

	/**
	 * @param age the age to set
	 */
	public void setAge(Long age) {
		this.age = age;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the user_level
	 */
	public String getUser_level() {
		return user_level;
	}

	/**
	 * @param user_level the user_level to set
	 */
	public void setUser_level(String user_level) {
		this.user_level = user_level;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * @param cpf the cpf to set
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
