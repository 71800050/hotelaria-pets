/**
 * 
 */
package br.com.cotemig.petshostel.dto;

import java.io.Serializable;

import br.com.cotemig.petshostel.entities.Endereco;

/**
 * @author Luiz Diamantino
 *
 */


public class EnderecoDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String rua;
	private String numero;
	private String uf;
	
	public EnderecoDTO() {}

	/**
	 * @param id
	 * @param rua
	 * @param numero
	 * @param uf
	 */
	public EnderecoDTO(Long id, String rua, String numero, String uf) {
		this.id = id;
		this.rua = rua;
		this.numero = numero;
		this.uf = uf;
	}
	
	/**
	 * @param entity (Endereco)
	 */
	public EnderecoDTO(Endereco entity) {
		this.id = entity.getId();
		this.rua = entity.getRua();
		this.numero = entity.getNumero();
		this.uf = entity.getUf();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the rua
	 */
	public String getRua() {
		return rua;
	}

	/**
	 * @param rua the rua to set
	 */
	public void setRua(String Rua) {
		this.rua = rua;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the uf
	 */
	public String getUf() {
		return uf;
	}

	/**
	 * @param ufthe uf to set
	 */
	public void setUf(String uf) {
		this.uf = uf;
	}

}
