/**
 * 
 */
package br.com.cotemig.petshostel.dto;

import java.io.Serializable;

import br.com.cotemig.petshostel.entities.Pessoa;

/**
 * @author Luiz Diamantino
 *
 */


public class PessoaInsertDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name;
	private String cpf;

	public PessoaInsertDTO() {}

	/**
	 * @param name
	 * @param cpf
	 */
	public PessoaInsertDTO(String name, String cpf) {
		this.name = name;
		this.cpf = cpf;
	}

	/**
	 * @param entity (Pessoa)
	 */
	public PessoaInsertDTO(Pessoa entity) {
		this.name = entity.getName();
		this.cpf = entity.getCpf();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * @param cpf the cpf to set
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
