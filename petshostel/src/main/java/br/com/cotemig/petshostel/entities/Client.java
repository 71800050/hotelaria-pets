/**
 * 
 */
package br.com.cotemig.petshostel.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.OneToMany;

/**
 * @author José Eustaquio Muniz
 *
 */
public class Client extends User implements Serializable {
	private static final long serialVersionUID = 1L;

	@OneToMany(mappedBy = "client")
	private List<Pet> pets = new ArrayList<>();

	public Client() {}

	/**
	 * @param id
	 * @param cpf
	 * @param name
	 * @param gender
	 * @param age
	 * @param phone
	 * @param address
	 * @param email
	 * @param password
	 * @param user_level
	 */
	public Client(Long id, String cpf, String name, String gender, Long age, String phone, String address, String email,
			String password, String user_level) {
		super(id, cpf, name, gender, age, phone, address, email, password, user_level);		
	}

	/**
	 * @return the pets
	 */
	public List<Pet> getPets() {
		return pets;
	}

}
