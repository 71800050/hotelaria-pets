/**
 * 
 */
package br.com.cotemig.petshostel.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cotemig.petshostel.dto.PessoaDTO;
import br.com.cotemig.petshostel.dto.PessoaInsertDTO;
import br.com.cotemig.petshostel.entities.Pessoa;
import br.com.cotemig.petshostel.repositories.PessoaRepository;

/**
 * @author Luiz Diamantino
 *
 */


@Service
public class PessoaService {

	@Autowired
	private PessoaRepository pessoaRepository;

	// https://blog.algaworks.com/chega-de-nullpointerexception/
	public PessoaDTO findById(Long id) {
		Optional<Pessoa> pessoa = Optional.ofNullable(pessoaRepository.findById(id).get());

		if (pessoa.isPresent())
			return new PessoaDTO(pessoa.get());
		return null;
	}

	public List<PessoaDTO> findAll() {
		List<Pessoa> result = pessoaRepository.findAll();

		return result.stream().map(x -> new PessoaDTO(x)).collect(Collectors.toList());
	}
	
	public PessoaDTO insert(PessoaInsertDTO dto) {
		Pessoa pessoa = pessoaRepository.findByCpf(dto.getCpf());
		if (pessoa != null) {
			throw new ServiceException("CPF já existe");
		}

		Pessoa obj = new Pessoa();
		obj.setName(dto.getName());
		obj.setCpf(dto.getCpf());
		
		obj = pessoaRepository.save(obj);
		
		return new PessoaDTO(obj);
	}

	/*
	 * @Transactional(readOnly = true)
	 * public List<SaleSumDTO> amountGroupedBySeller() {
	 *     return repository.amountGroupedBySeller();
	 * }
	 */

}
