/**
 * 
 */
package br.com.cotemig.petshostel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cotemig.petshostel.entities.Pet;

/**
 * @author Luiz Diamantino
 *
 */


public interface PetRepository extends JpaRepository<User, Long> {

	Pet findBySpecies(String species);

}
