/**
 * 
 */
package br.com.cotemig.petshostel.entities;

import java.io.Serializable;

/**
 * @author José Eustaquio Muniz
 *
 */
// ATENDENTE
public class Clerk extends User implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String matricula;

	public Clerk() {}

	/**
	 * @param id
	 * @param cpf
	 * @param name
	 * @param gender
	 * @param age
	 * @param phone
	 * @param address
	 * @param email
	 * @param password
	 * @param user_level
	 */
	public Clerk(Long id, String cpf, String name, String gender, Long age, String phone, String address, String email,
			String password, String user_level, String matricula) {
		super(id, cpf, name, gender, age, phone, address, email, password, user_level);
		
		this.matricula = matricula;
	}

	/**
	 * @return the matricula
	 */
	public String getMatricula() {
		return matricula;
	}

	/**
	 * @param matricula the matricula to set
	 */
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

}
