/**
 * 
 */
package br.com.cotemig.petshostel.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cotemig.petshostel.dto.PettDTO;
import br.com.cotemig.petshostel.dto.PetInsertDTO;
import br.com.cotemig.petshostel.entities.Pet;
import br.com.cotemig.petshostel.repositories.PetRepository;

/**
 * @author Luiz Diamantino
 *
 */

@Service
public class PetService {

	@Autowired
	private PetRepository petRepository;

	// https://blog.algaworks.com/chega-de-nullpointerexception/
	public PetDTO findById(Long id) {
		Optional<Pet> pet = Optional.ofNullable(petRepository.findById(id).get());

		if (pet.isPresent())
			return new PetDTO(pet.get());
		return null;
	}

	public List<PetDTO> findAll() {
		List<Pet> result = petRepository.findAll();

		return result.stream().map(x -> new PetDTO(x)).collect(Collectors.toList());
	}
	
	public PetDTO insert(PetInsertDTO dto) {

		Pet obj = new Pet();
		obj.setName(dto.getName());
		obj.setSpecies(dto.getSpecies());
		
		obj = petRepository.save(obj);
		
		return new speciesDTO(obj);
	}

	/*
	 * @Transactional(readOnly = true)
	 * public List<SaleSumDTO> amountGroupedBySeller() {
	 *     return repository.amountGroupedBySeller();
	 * }
	 */

}
