/**
 * 
 */
package br.com.cotemig.petshostel.dto;

import java.io.Serializable;

import br.com.cotemig.petshostel.entities.Pet;

/**
 * @author Luiz Diamantino
 *
 */


public class PetInsertDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name;
	private String species;

	public PetInsertDTO() {}

	/**
	 * @param name
	 * @param species
	 */
	public PetInsertDTO(String name, String species) {
		this.name = name;
		this.species = species;
	}

	/**
	 * @param entity (Pet)
	 */
	public PetInsertDTO(Pet entity) {
		this.name = entity.getName();
		this.species = entity.getSpecies();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the species
	 */
	public String getSpecies() {
		return species;
	}

	/**
	 * @param species the species to set
	 */
	public void setSpecies(String species) {
		this.species = species;
	}

}
