/**
 * 
 */
package br.com.cotemig.petshostel.dto;

import java.io.Serializable;

import br.com.cotemig.petshostel.entities.Endereco;

/**
 * @author Luiz Diamantino
 *
 */


public class EnderecoInsertDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String rua;
	private String numero;
	private String uf;

	public EnderecoInsertDTO() {}

	/**
	 * @param rua
	 * @param numero
	 * @param uf
	 */
	public EnderecoInsertDTO(String rua, String numero, String uf) {
		this.rua = rua;
		this.numero = numero;
		this.uf = uf;
	}

	/**
	 * @param entity (Endereco)
	 */
	public EnderecoInsertDTO(Endereco entity) {

		this.rua = entity.getRua();
		this.numero = entity.getNumero();
		this.uf = entity.getUf();
	}

	/**
	 * @return the rua
	 */
	public String getRua() {
		return rua;
	}

	/**
	 * @param rua the rua to set
	 */
	public void setRua(String Rua) {
		this.rua = rua;
	}

	/**
	 * @return the numero
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * @param numero the numero to set
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * @return the uf
	 */
	public String getUf() {
		return uf;
	}

	/**
	 * @param ufthe uf to set
	 */
	public void setUf(String uf) {
		this.uf = uf;
	}

}
