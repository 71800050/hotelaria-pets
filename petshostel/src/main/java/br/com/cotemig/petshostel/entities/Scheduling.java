/**
 * 
 */
package br.com.cotemig.petshostel.entities;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @author José Eustaquio Muniz
 *
 */
/* @ Entity
@ Table(name = "tb_scheduling") */
public class Scheduling {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
	private Long id;
	private String scheduled_date; // data_agendada
	private String schedule;       // horario
	private String service;        // servico;
	private String order;          // pedido;

	/* private Client pet_owner;         //usuario_id */

	public Scheduling() {}

	/**
	 * @param id
	 * @param scheduled_date
	 * @param schedule
	 * @param service
	 * @param order
	 * 
	 */
	public Scheduling(Long id, String scheduled_date, String schedule, String service, String order) { /*, Client pet_owner*/
		this.id = id;
		this.scheduled_date = scheduled_date;
		this.schedule = schedule;
		this.service = service;
		this.order = order;
		// this.pet_owner = pet_owner;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the scheduled_date
	 */
	public String getScheduled_date() {
		return scheduled_date;
	}

	/**
	 * @param scheduled_date the scheduled_date to set
	 */
	public void setScheduled_date(String scheduled_date) {
		this.scheduled_date = scheduled_date;
	}

	/**
	 * @return the schedule
	 */
	public String getSchedule() {
		return schedule;
	}

	/**
	 * @param schedule the schedule to set
	 */
	public void setSchedule(String schedule) {
		this.schedule = schedule;
	}

	/**
	 * @return the service
	 */
	public String getService() {
		return service;
	}

	/**
	 * @param service the service to set
	 */
	public void setService(String service) {
		this.service = service;
	}

	/**
	 * @return the order
	 */
	public String getOrder() {
		return order;
	}

	/**
	 * @param order the order to set
	 */
	public void setOrder(String order) {
		this.order = order;
	}

	/**
	 * @return the pet_owner
	 */
	/*	public Client getPet_owner() {
		return pet_owner;
	}
	*/
	/**
	 * @param pet_owner the pet_owner to set
	 */
	/* public void setPet_owner(Client pet_owner) {
		this.pet_owner = pet_owner;
	}
*/
}
