/**
 * 
 */
package br.com.cotemig.petshostel.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cotemig.petshostel.dto.UserDTO;
import br.com.cotemig.petshostel.dto.UserInsertDTO;
import br.com.cotemig.petshostel.entities.User;
import br.com.cotemig.petshostel.repositories.UserRepository;

/**
 * @author Luiz Diamantino
 *
 */

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	// https://blog.algaworks.com/chega-de-nullpointerexception/
	public UserDTO findById(Long id) {
		Optional<User> user = Optional.ofNullable(userRepository.findById(id).get());

		if (user.isPresent())
			return new UserDTO(user.get());
		return null;
	}

	public List<UserDTO> findAll() {
		List<User> result = userRepository.findAll();

		return result.stream().map(x -> new UserDTO(x)).collect(Collectors.toList());
	}

	public UserDTO insert(UserInsertDTO dto) {
		/* User user = userRepository.findByEmail(dto.getEmail());
		if (user != null) {
			throw new ServiceException("Email já existe");
		}

		User obj = new User();
		obj.setName(dto.getName());
		obj.setEmail(dto.getEmail());

		obj = userRepository.save(obj);

		return new UserDTO(obj); */
		return null;
	}

	/*
	 * @Transactional(readOnly = true)
	 * public List<SaleSumDTO> amountGroupedBySeller() {
	 *     return repository.amountGroupedBySeller();
	 * }
	 */

}