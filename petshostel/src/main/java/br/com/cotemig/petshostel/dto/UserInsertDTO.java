/**
 * 
 */
package br.com.cotemig.petshostel.dto;

import java.io.Serializable;

import br.com.cotemig.petshostel.entities.User;

/**
 * @author Luiz Diamantino
 *
 */


public class UserInsertDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name;
	private String email;

	public UserInsertDTO() {}

	/**
	 * @param name
	 * @param email
	 */
	public UserInsertDTO(String name, String email) {
		this.name = name;
		this.email = email;
	}

	/**
	 * @param entity (User)
	 */
	public UserInsertDTO(User entity) {
		this.name = entity.getName();
		this.email = entity.getEmail();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

}
