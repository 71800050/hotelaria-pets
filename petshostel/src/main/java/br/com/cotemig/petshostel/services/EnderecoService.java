/**
 * 
 */
package br.com.cotemig.petshostel.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cotemig.petshostel.dto.EnderecoDTO;
import br.com.cotemig.petshostel.dto.EnderecoInsertDTO;
import br.com.cotemig.petshostel.entities.Endereco;
import br.com.cotemig.petshostel.repositories.EnderecoRepository;

/**
 * @author Luiz Diamantino
 *
 */


@Service
public class EnderecoService {

	@Autowired
	private EnderecoRepository enderecoRepository;

	// https://blog.algaworks.com/chega-de-nullpointerexception/
	public EnderecoDTO findById(Long id) {
		Optional<Endereco> endereco = Optional.ofNullable(enderecoRepository.findById(id).get());

		if (endereco.isPresent())
			return new EnderecoDTO(endereco.get());
		return null;
	}

	public List<EnderecoDTO> findAll() {
		List<Endereco> result = enderecoRepository.findAll();

		return result.stream().map(x -> new EnderecoDTO(x)).collect(Collectors.toList());
	}

	public EnderecoDTO insert(EnderecoInsertDTO dto) {

		Endereco obj = new Endereco();
		obj.setRua(dto.getRua());
		obj.setNumero(dto.getNumero());
		obj.setUf(dto.getUf());

		obj = enderecoRepository.save(obj);

		return new EnderecoDTO(obj);
	}

	/*
	 * @Transactional(readOnly = true)
	 * public List<SaleSumDTO> amountGroupedBySeller() {
	 *     return repository.amountGroupedBySeller();
	 * }
	 */

}