/**
 * 
 */
package br.com.cotemig.petshostel.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cotemig.petshostel.entities.Pessoa;
import br.com.cotemig.petshostel.repositories.PessoaRepository;

/**
 * @author Brian Alves Pereira / Luiz Diamantino
 *
 */
@RestController
@RequestMapping(value = "/pessoas")
public class PessoaController {

	@Autowired
	private PessoaRepository repository;

	@GetMapping(value = "/{id}")
	public ResponseEntity<Pessoa> findById(@PathVariable Long id) {
		Pessoa pessoa = repository.findById(id).get();
		return ResponseEntity.ok(pessoa);
	}

	@GetMapping
	public ResponseEntity<List<PessoaDTO>> findAll() {
		List<PessoaDTO> list = pessoaService.findAll();

		return ResponseEntity.ok(list);
	}

	@PostMapping
	public ResponseEntity<PessoaDTO> insert(@RequestBody PessoaInsertDTO dto) {
		try {
			PessoaDTO obj = pessoaService.insert(dto);
			URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(obj.getId()).toUri();

			return ResponseEntity.created(uri).body(obj);
		} catch (ServiceException e) {
			return ResponseEntity.unprocessableEntity().build();
		}
	}

}
