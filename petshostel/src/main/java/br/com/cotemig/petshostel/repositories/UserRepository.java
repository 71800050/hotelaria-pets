/**
 * 
 */
package br.com.cotemig.petshostel.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.cotemig.petshostel.entities.User;

/**
 * @author Luiz Diamantino
 *
 */


public interface UserRepository extends JpaRepository<User, Long> {

	User findByEmail(String email);

}
