/**
 * 
 */
package br.com.cotemig.petshostel.dto;

import java.io.Serializable;

import br.com.cotemig.petshostel.entities.Pet;

/**
 * @author Luiz Diamantino
 *
 */


public class PetDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String name;
	private String species;
	
	public PetDTO() {}

	/**
	 * @param id
	 * @param name
	 * @param species
	 */
	public PetDTO(Long id, String name, String species) {
		this.id = id;
		this.name = name;
		this.species = species;
	}
	
	/**
	 * @param entity (Pet)
	 */
	public PetDTO(Pet entity) {
		this.id = entity.getId();
		this.name = entity.getName();
		this.species = entity.getSpecies();
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the species
	 */
	public String getSpecies() {
		return species;
	}

	/**
	 * @param species the species to set
	 */
	public void setSpecies(String species) {
		this.species = species;
	}

}
