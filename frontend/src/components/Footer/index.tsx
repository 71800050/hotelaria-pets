const Footer = () => {
  return (
    <footer className="footer mt-auto py-3 bg-dark">
      <div className="container">
        <p className="text-light">
          App desenvolvido por <a href="https://github.com/jemsantos" target="_blank" rel="noreferrer">José Eustaquio</a>
        </p>
        <p className="text-light">
          <small>
            <strong>Pets Hostel</strong><br />
            Trabalho disciplina Qualidade Software - SI / <a href="http://www.cotemig.com.br/" target="_blank" rel="noreferrer">@COTEMIG</a>
          </small>
        </p>
      </div>
    </footer>
  );
};

export default Footer;
